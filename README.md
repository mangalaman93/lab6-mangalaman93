# CSE 6230, Fall 2014: Lab 6: UVA/UVM and MPI+CUDA #

Question 1
==========
* Reversal with explicit copies -> 3.64 GB/s
* Reversal without explicit copies (pinned) -> 8.14 GB/s

Question 2
==========
* GPU-to-GPU copy (buffered through CPU) memory -> 2.22 GB/s
* GPU-to-GPU direct copy -> 2.99 GB/s

Question 3
==========
* CPU -> Nodes used: jinx2, jinx3; problem size: 4096; Effective GFLOP/s: 215.5
* GPU -> Nodes used: jinx2, jinx3; problem size: 4096; Effective GFLOP/s: 661.7

Question 4
==========
* Nodes used: jinx2, jinx3; problem size: 4096;
* Pinned GPU -> Effective TFLOP/s: 2
* Pinned GPU (with device synchronization) -> Effective GFLOP/s: 11
* Pinned GPU (with explicit transfer only) -> Effective GFLOP/s: 699.0
